# ABCstar-Irradiation


in your workarea

export $workarea=$PWD;

git clone https://gitlab.cern.ch/hwang43/abcstar-irradiation.git
mkdir testsetup/data;
mkdir testsetup/ps;
mkdir testsetup/results;
mkdir testsetup/hsio;
mkdir testsetup/timers;

git clone https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw.git 
cd itsdaq-sw;
cp ../RegisterReadBack.C .;

export SCTDAQ_VAR=../testsetup
source /<your_root_dir>/thisroot.sh;

# Install

python waf configure;

python waf build;

python waf install;

# HSIO

../setup_fpga.sh;

the following should be done in a separate terminal

sudo su;

cd $workarea/itsdaq-sw/;

bin/hsioPipe --eth eno1,e0:dd:cc:bb:aa:00 --file /tmp/hsioPipe.toHsio,/tmp/hsioPipe.fromHsio &

#RUN ITSDAQ

./RUNITSDAQ.sh

or in batch mode 

./BatchMode_RUNITSDAQ.sh
#!/bin/bash

#
# Simple script to configure the NEXYS Video FPGA
# Written by Matthew Gignac (SCIPP)
#

rm -rf /tmp/hsioPipe.*
mkfifo /tmp/hsioPipe.toHsio
mkfifo /tmp/hsioPipe.fromHsio

# Set paths first
#source inits/setup.sh #June27,update: configured in .bashrc

## Configure FGPA
echo " >>>>>>>>> LISTING FGPA DEVICES <<<<<<<<< " 
djtgcfg enum

echo " >>>>>>>>> INITIALIZING THE NEXYSVIDEO DEVICE <<<<<<<<< "
djtgcfg init -d NexysVideo

echo " >>>>>>>>> PROGRAMMING THE NEXYSVIDEO DEVICE <<<<<<<<< "
# Peter Phillips and Denis suggested to use FSC version of the firmware for the Single Chip Board
djtgcfg prog -d NexysVideo -i 0 --file nexysv_itsdaq_vb2e6_FSC_STAR.bit
#djtgcfg prog -d NexysVideo -i 0 --file nexysv_itsdaq_vb2b3_FSC_STAR.bit

#http://www.hep.ucl.ac.uk/~warren/upgrade/firmware/?C=M;O=D

Configuration files
===================

These files are provided as example configurations for the ITSDAQ 
software to use online. It reads these from $SCTDAQ_VAR/config,
so files should be placed there and modified as appropriate.

System configuration
--------------------

The system configuration mapping are described in the file
st_system_config.dat. This includes both the conncetion to the
DAQ and the stream configuration for the modules. The module
configurations are described in separate files in the same
directory, linked by name.

Detector configuration
----------------------

The chip level configurations are described by .det files, which 
are named in the system configuration.

A variety of examples are provided here for different hybrid
geometries.
If "mymodule.det" does not exist, the "default.det" file (in sctvar)
will be used.

Strip masks can be loaded from files list "mymodule.mask". This is a
list of channels to be masked, one per line. This is interpreted as
pairs of numbers, the first being the chip address and the second 
being the channel number within the chip.
The same information can also be included in the detector configuration
file, see examples for a description.

Trims are loaded from trim files "mymodule.trim" (description to be added).
  
Trigger configuration
---------------------

The slog*.txt files (also those in the triggers directory) allow bit-by-bit
configuration of trigger patterns and are not restricted to use on SLOG.
The [test vectors](vectors/abc130) used for ABC130 wafer testing are also
stored in a similar format.

GBTX
----

There are preliminary configs for [GBTX](config/gbtx/README.md), including python scripts
for reading and writing files in the XML format and raw register
data (.txt files).

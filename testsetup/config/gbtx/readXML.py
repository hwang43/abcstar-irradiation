#!/usr/bin/python

#This function opens xml files from the CERN GBTx configuration website and turns 
#the data into a vector of register values. 
#This vector of register values can be sent over the slow control link to a GBTx
#chip. See sendSlowControl.py.
#The GBTx can not be fully configured over a slow control link without blowing 
#fuses. This problem is not properly documented. For now, the plan is to configure
#a GBTx chip over I2C, then re-configure over slow control. Once we know which fuses to
#blow, then all configuration can be done over slow control.

from xml.dom.minidom import parse
import xml.dom.minidom


def readXMLfromCern(f):
	# Open XML document using minidom parser
	DOMTree = xml.dom.minidom.parse(f)
	collection = DOMTree.documentElement

	# Get all the signals in the collection
	SignalList = collection.getElementsByTagName("Signal")

	# Loop through listing of signals and add data to the array
	SignalArray = [0] * 366#number of registers
	#not declared as an 8 bit number. should be OK as long as no bad data is fed in.
	for sig in SignalList:
		sigName = sig.getAttribute("name")
		sigNumberBits = int(float(sig.getAttribute("numberBits")))
		sigValue = int(float(sig.getElementsByTagName('value')[0].childNodes[0].data))
		sigNumLocations = int(float(sig.getElementsByTagName('location').length)) #some fields are triplicated
		# Loop to handle triplicated signals (or any other signals stored in 1 or more registers)
		for n in range(0, sigNumLocations):
			tmp = sigValue #read value from xml
			sigBitShift = int(float(sig.getElementsByTagName('location')[n].getAttribute("startBitIndex")))
			sigAddress = int(float(sig.getElementsByTagName('location')[n].getAttribute("startAddress")))
			tmp = tmp<<sigBitShift #bit shift value to get to correct bit location
			#add some error catching code here in case of bad data
			SignalArray[sigAddress] = SignalArray[sigAddress] | tmp #use or function to avoid changing other bits of number
	for n in SignalArray:
		if n>256:
			print ("ERROR bad value  %s" %n)

	return SignalArray

def interpretUsingXML(f, data):
	"""
	  Read register map from XML, and interpret the configuration bytes.
	    f is an XML file, data is a list of 366 integers (register bytes)
	""" 

	# Open XML document using minidom parser
	DOMTree = xml.dom.minidom.parse(f)
	collection = DOMTree.documentElement

	# Get all the signals in the collection
	SignalList = collection.getElementsByTagName("Signal")

	# Loop through listing of signals and read data
	for sig in SignalList:
		sigName = sig.getAttribute("name")
		sigNumberBits = int(float(sig.getAttribute("numberBits")))
		sigNumLocations = int(float(sig.getElementsByTagName('location').length)) #some fields are triplicated
		# Loop to handle triplicated signals (or any other signals stored in 1 or more registers)
		numbers = []
		for n in range(0, sigNumLocations):
			sigBitShift = int(float(sig.getElementsByTagName('location')[n].getAttribute("startBitIndex")))
			sigLastBit = int(float(sig.getElementsByTagName('location')[n].getAttribute("lastBitIndex")))
			sigAddress = int(float(sig.getElementsByTagName('location')[n].getAttribute("startAddress")))
			value = data[sigAddress]
			value = value >> sigBitShift #bit shift value to get to correct bit location
			nBits = sigLastBit - sigBitShift + 1
			mask = ((1 << nBits)-1) & 0xff
			value = value & mask # Mask off other bits
			numbers.append(value)
		print("%-30s:" % sigName, numbers)

def readRawData(f):
	input = open(f)
	data = [0] * 366
	for i, l in enumerate(input):
		data[i] = int(l, 16)
	return data

# As an example process this directory
if __name__ == "__main__":
	data = readXMLfromCern("Loopback_test.xml")
	print("\n".join("%02x" % d for d in data))

	print("Reinterpret loopback test data")
	interpretUsingXML("Loopback_test.xml", data)

	print("Reinterpret minimal config data")
	data = readRawData("MinimalConfig.txt")
	interpretUsingXML("Loopback_test.xml", data)

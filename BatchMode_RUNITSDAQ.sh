#!/bin/bash
echo Running ITS DAQ

# Should work with both ./RUNITSDAQ.sh and . ./RUNITSDAQ.sh
source_name=${BASH_SOURCE[0]}

if [ "x${source_name}" = "x" ]; then
  echo "Using SCTDAQ_ROOT=$SCTDAQ_ROOT (from env)"
else
  which realpath 2> /dev/null > /dev/null && export SCTDAQ_ROOT=$(realpath $(dirname ${source_name}))
  which realpath 2> /dev/null > /dev/null || export SCTDAQ_ROOT=$(dirname ${source_name})
  if [ "${SCTDAQ_ROOT}" = "." ]; then
    export SCTDAQ_ROOT=$(pwd)
  fi
  echo "Using SCTDAQ_ROOT=$SCTDAQ_ROOT (from script directory)"
fi

if [[ "$ROOTSYS" == "" && -x /usr/bin/root ]] ; then
  echo "Using system ROOT"
  export ROOTSYS=/usr
fi

# Set RUN_ROOT6=yes if using root6
function check_root_version_6()
{
  RUN_ROOT6=no

  # If root-config is in the path this is easy
  if [[ -x $(which root-config) ]]
  then
    if root-config --version | grep "^6.*" > /dev/null
    then
      RUN_ROOT6=yes
      return
    fi
  fi

  # If no root-config look for clues in the ROOTSYS path
  if [[ "x$(echo $ROOTSYS | sed 's:.*ROOT/\(.\).*:\1:')" = "x6" || "x$(echo $ROOTSYS | sed 's:.*\(root-6\).*:\1:')" = "xroot-6" ]]; then
    RUN_ROOT6=yes
  fi
}

check_root_version_6

RUN_DIR=$SCTDAQ_ROOT

if [[ $RUN_ROOT6 == yes ]]; then
  echo Using root 6 setup
  RUN_DIR=$SCTDAQ_ROOT/root6

  # Macros expect to be able to refer to things from top directory
  export ROOT_INCLUDE_PATH=$SCTDAQ_ROOT
fi

[[ "$SCTDAQ_VAR" == "" ]] && echo -e '\033[31mPlease set SCTDAQ_VAR to point to config area\033[39m'

echo "Now running Stavelet macro in ROOT"
(cd $RUN_DIR; $ROOTSYS/bin/root -b Stavelet.cpp)
